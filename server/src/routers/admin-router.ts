/*
 * Copyright (C) 2022-2025 Savoir-faire Linux Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
import argon2 from 'argon2'
import envPaths from 'env-paths'
import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { ParamsDictionary, Request, Response } from 'express-serve-static-core'
import fs from 'fs'
import { readdir, stat } from 'fs/promises'
import {
  AccessToken,
  AccountOverview,
  AdminCredentials,
  AdminWizardState,
  HttpStatusCode,
  isMailAddress,
} from 'jami-web-common'
import path from 'path'
import { Container } from 'typedi'

import { Jamid } from '../jamid/jamid.js'
import { adminAuthenticationMiddleware } from '../middleware/auth.js'
import { checkAdminSetup } from '../middleware/setup.js'
import { Accounts } from '../storage/accounts.js'
import { AdminAccount } from '../storage/admin-account.js'
import { OpenIdProviders } from '../storage/openid-providers.js'
import { isConfigValid } from '../utils/admin.js'
import { signJwt } from '../utils/jwt.js'

const jamid = Container.get(Jamid)
const adminAccount = Container.get(AdminAccount)
const accounts = Container.get(Accounts)
const providersSupport = Container.get(OpenIdProviders)

export const adminRouter = Router()

const paths = envPaths('jami', { suffix: '' })
const dataPath = paths.data

const multerPath = adminAccount.getUploadPath()
if (!fs.existsSync(multerPath)) {
  fs.mkdirSync(multerPath)
}

adminRouter.get('/check', (_req, res) => {
  const isSetupComplete = adminAccount.getPasswordHash() !== undefined
  res.send({ isSetupComplete })
})

adminRouter.get('/adminWizardState', (_req, res) => {
  const lastWizardState = adminAccount.getLastWizardState()
  res.send({ lastWizardState })
})

adminRouter.post(
  '/create',
  asyncHandler(async (req: Request<ParamsDictionary, AccessToken | string, Partial<AdminCredentials>>, res) => {
    const { password } = req.body
    if (password) {
      const hashedPassword = await argon2.hash(password, { type: argon2.argon2id })

      adminAccount.setPasswordHash(hashedPassword)
      await adminAccount.save()

      const jwt = await signJwt('admin')
      adminAccount.setLastWizardState(AdminWizardState.local)
      res.send({ accessToken: jwt })
      return
    }

    res.status(HttpStatusCode.BadRequest).send('Invalid password')
  }),
)
//Requests below require a successful setup
adminRouter.use(checkAdminSetup)

adminRouter.post('/adminWizardState', (req: Request<ParamsDictionary, AdminWizardState>, res) => {
  const lastWizardState = req.body.state
  adminAccount.setLastWizardState(lastWizardState)
  res.sendStatus(HttpStatusCode.Ok)
})

adminRouter.post(
  '/login',
  asyncHandler(async (req: Request<ParamsDictionary, AccessToken | string, Partial<AdminCredentials>>, res) => {
    const { password } = req.body
    if (password === undefined) {
      res.status(HttpStatusCode.BadRequest).send('Missing password in body')
      return
    }

    const hashedPassword = adminAccount.getPasswordHash()
    if (hashedPassword === undefined) {
      res.status(HttpStatusCode.InternalServerError).send('Admin password not found')
      return
    }

    const isPasswordVerified = await argon2.verify(hashedPassword, password)
    if (!isPasswordVerified) {
      res.status(HttpStatusCode.Unauthorized).send('Incorrect password')
      return
    }

    const jwt = await signJwt('admin')
    res.send({ accessToken: jwt })
  }),
)

adminRouter.get(
  '/config',
  asyncHandler(async (_, res: Response) => {
    if (adminAccount.getOpenIdProvidersNames().length === 0) {
      adminAccount.updateConfig({ openIdAuthEnabled: false })
    }

    const config = adminAccount.getConfig()
    if (config !== undefined) {
      res.status(HttpStatusCode.Ok).send(config)
      return
    }
    res.sendStatus(HttpStatusCode.InternalServerError)
  }),
)

//Requests below require authentication
adminRouter.use(adminAuthenticationMiddleware)

adminRouter.get(
  '/isTokenValid',
  asyncHandler(async (_, res: Response) => {
    // If the request reaches this point, the token is valid
    // The token is verified by the adminAuthenticationMiddleware
    res.sendStatus(HttpStatusCode.Ok)
  }),
)

adminRouter.patch(
  '/config',
  asyncHandler(async (req: Request<ParamsDictionary, unknown, unknown>, res: Response) => {
    const config = req.body

    if (!isConfigValid(config)) {
      res.status(HttpStatusCode.BadRequest).send('Invalid config in the payload')
      return
    }
    adminAccount.updateConfig(config)
    try {
      await adminAccount.save()
      res.sendStatus(HttpStatusCode.Ok)
    } catch (e) {
      res.sendStatus(HttpStatusCode.InternalServerError)
    }
  }),
)

//Requests below require authentication
adminRouter.use(adminAuthenticationMiddleware)

adminRouter.patch(
  '/password',
  asyncHandler(async (req, res) => {
    const newPassword = req.body.new
    const oldPassword = req.body.old

    const oldPasswordHash = adminAccount.getPasswordHash()
    if (oldPasswordHash === undefined) {
      res.status(HttpStatusCode.InternalServerError).send('Admin password not found')
      return
    }

    const isPasswordVerified = await argon2.verify(oldPasswordHash, oldPassword)
    if (!isPasswordVerified) {
      res.status(HttpStatusCode.Unauthorized).send('Incorrect password')
      return
    }

    if (newPassword === undefined) {
      res.status(HttpStatusCode.BadRequest).send('Missing password in body')
      return
    }
    if (newPassword) {
      try {
        const hashedPassword = await argon2.hash(newPassword, { type: argon2.argon2id })
        adminAccount.setPasswordHash(hashedPassword)
        await adminAccount.save()
        res.sendStatus(HttpStatusCode.Ok)
        return
      } catch (e) {
        res.sendStatus(HttpStatusCode.InternalServerError)
        return
      }
    }
    res.sendStatus(HttpStatusCode.BadRequest)
  }),
)

adminRouter.post(
  '/nameserver',
  asyncHandler(async (req, res) => {
    const nameserver = req.body.nameserver
    // create JSON file is not exists and set the name server address in it
    // replace it if it exists
    // this overrides the default name server address in the daemon
    // https://ns.jami.net

    if (!nameserver || nameserver === '') {
      res.status(HttpStatusCode.BadRequest).send('Nameserver not provided')
      return
    }
    if (!/^(http(s)?:\/\/)?([a-zA-Z0-9\-.]+)(:\d{1,5})?(\/.*)?$/g.test(nameserver)) {
      res.status(HttpStatusCode.BadRequest).send('Invalid nameserver')
      return
    }
    adminAccount.setNameServerAdress(nameserver)
    adminAccount.save()
    res.sendStatus(HttpStatusCode.Ok)
  }),
)

adminRouter.delete(
  '/nameserver',
  asyncHandler(async (req, res) => {
    adminAccount.resetNameServerAdress()
    adminAccount.save()
    res.sendStatus(HttpStatusCode.Ok)
  }),
)

adminRouter.get(
  '/nameserver',
  asyncHandler(async (req, res) => {
    const nameserver = adminAccount.getNameServerAddress()
    if (nameserver === '') {
      // default name server address
      res.send('https://ns.jami.net')
      return
    }
    res.send(nameserver)
  }),
)

adminRouter.post(
  '/jamsurl',
  asyncHandler(async (req, res) => {
    const jamsUrl = req.body.url

    if (!jamsUrl || jamsUrl === '') {
      res.status(HttpStatusCode.BadRequest).send('Nameserver not provided')
      return
    }
    if (!/^(http(s)?:\/\/)?([a-zA-Z0-9\-.]+)(:\d{1,5})?(\/.*)?$/g.test(jamsUrl)) {
      res.status(HttpStatusCode.BadRequest).send('Invalid url')
      return
    }
    adminAccount.setJamsServerUrl(jamsUrl)
    adminAccount.save()
    res.sendStatus(HttpStatusCode.Ok)
  }),
)

adminRouter.delete(
  '/jamsurl',
  asyncHandler(async (req, res) => {
    adminAccount.resetJamsServerUrl()
    adminAccount.save()
    adminAccount.updateConfig({ jamsAuthEnabled: false })
    res.sendStatus(HttpStatusCode.Ok)
  }),
)

adminRouter.get(
  '/jamsurl',
  asyncHandler(async (req, res) => {
    const url = adminAccount.getJamsServerUrl()
    if (url === undefined) {
      res.status(HttpStatusCode.NoContent).send('JAMS server URL is not set')
      return
    }
    res.send(url)
  }),
)

const getSizeOnDisk = async (directoryPath: string): Promise<number> => {
  try {
    const files = await readdir(directoryPath)
    const sizes = await Promise.all(
      files.map(async (file) => {
        const filePath = path.join(directoryPath, file)
        const stats = await stat(filePath)

        if (stats.isDirectory()) {
          return getSizeOnDisk(filePath)
        }
        return stats.size
      }),
    )

    return sizes.reduce((total, size) => total + size, 0)
  } catch {
    console.error('Directory not found')
    return 0
  }
}

adminRouter.get(
  '/accounts',
  asyncHandler(async (req, res) => {
    const allAccounts = accounts.getAll()
    const localAccounts = allAccounts.local
    const jamsAccounts = allAccounts.jams
    const openIdAccounts = allAccounts.openid
    let accountsList: AccountOverview[] = []

    for (const [username, account] of Object.entries(jamsAccounts)) {
      const accountUri = jamid.getAccountDetails(account.accountId)['Account.username']
      const path = dataPath + '/' + account.accountId
      if (accountUri) {
        accountsList.push({
          username,
          accountUri,
          auth: 'jams',
          storage: await getSizeOnDisk(path),
          accountId: account.accountId,
        })
      }
    }
    for (const [username] of Object.entries(localAccounts)) {
      const accountId = jamid.getAccountIdFromUsername(username)
      if (accountId) {
        const accountUri = jamid.getAccountDetails(accountId)['Account.username']
        const path = dataPath + '/' + accountId
        if (accountUri) {
          accountsList.push({
            username,
            accountUri,
            auth: 'local',
            storage: await getSizeOnDisk(path),
            accountId,
          })
        }
      }
    }

    const alreadyAdded: Map<string, string> = new Map()
    for (const [username, account] of Object.entries(openIdAccounts)) {
      const accountUri = jamid.getAccountDetails(account.accountId)['Account.username']
      if (accountUri) {
        const addedAccount = alreadyAdded.get(account.accountId)
        if (addedAccount !== undefined && isMailAddress(addedAccount)) {
          continue
        }

        if (addedAccount !== undefined && !isMailAddress(addedAccount)) {
          accountsList = accountsList.filter((account) => account.username !== addedAccount)
        }
        const path = dataPath + '/' + account.accountId
        accountsList.push({
          username,
          accountUri,
          auth: account.service.join(', '),
          storage: await getSizeOnDisk(path),
          accountId: account.accountId,
        })
        alreadyAdded.set(account.accountId, username)
      }
    }

    for (const [accountId, expirationDate] of Object.entries(accounts.getGuests())) {
      const accountUri = jamid.getAccountDetails(accountId)['Account.username']
      if (accountUri) {
        const path = dataPath + '/' + accountId
        accountsList.push({
          username: accountId,
          accountUri,
          auth: 'guest',
          storage: await getSizeOnDisk(path),
          accountId,
          expirationDate: Number(expirationDate),
        })
        alreadyAdded.set(accountId, accountUri)
      }
    }

    res.send(accountsList)
  }),
)

adminRouter.delete(
  '/accounts',
  asyncHandler(async (req, res) => {
    const users = req.body
    for (const user of users) {
      const accountId = user.accountId
      jamid.removeAccount(accountId)
      accounts.remove(user.username, user.auth)
      const userFilePath = multerPath + accountId + '/'
      if (fs.existsSync(userFilePath)) {
        fs.rmdirSync(userFilePath, { recursive: true })
      }
    }
    res.sendStatus(HttpStatusCode.Ok)
  }),
)

adminRouter.get(
  '/oauth/providers',
  asyncHandler(async (_, res) => {
    const supportedProviders = providersSupport.getProviders()
    const data: { name: string; isActive: boolean; displayName: string }[] = []
    Array.from(supportedProviders).map(([key, values]) => {
      data.push({
        name: key,
        isActive: adminAccount.isProviderActive(key),
        displayName: values.displayName,
      })
    })
    res.send(data)
  }),
)

adminRouter.get(
  '/oauth/provider/:name',
  asyncHandler(async (req, res) => {
    const devMode = process.env.NODE_ENV === 'development'
    const provider = req.params.name
    const providerInfo = adminAccount.getOpenIdProvider(provider)
    const providerConfig = providersSupport.getOpenIdConfig(provider)

    const protocol = req.secure ? 'https' : 'http'
    const host = req.get('host')
    const baseUrl = devMode ? 'http://localhost:5002' : `${protocol}://${host}`

    if (providerConfig === undefined) {
      res.status(HttpStatusCode.InternalServerError).send('Provider configuration not found')
      return
    }

    if (providerInfo === undefined) {
      adminAccount.setOpenIdProvider(provider, '', '', false)
      adminAccount.save()
    }

    const data = {
      clientId: providerInfo?.clientId || '',
      isActive: adminAccount.isProviderActive(provider),
      redirectUri: baseUrl + providersSupport.getRedirectUri(provider),
      documentation: providerConfig.documentation,
      displayName: providerConfig.displayName,
    }

    res.send(data)
  }),
)

adminRouter.patch(
  '/oauth/provider/:name',
  asyncHandler(async (req, res) => {
    const provider = req.params.name
    const providers = adminAccount.getOpenIdProvidersNames()
    if (!providers.includes(provider)) {
      res.status(HttpStatusCode.BadRequest).send('Provider does not exist')
      return
    }

    const clientId = req.body.clientId
    const clientSecret = req.body.clientSecret
    const isActive = req.body.isActive

    if (clientId) {
      adminAccount.setOpenIdProviderClientId(provider, clientId)
    }

    if (clientSecret) {
      adminAccount.setOpenIdProviderClientSecret(provider, clientSecret)
    }

    if (isActive !== undefined) {
      if (isActive) {
        adminAccount.enableOpenIdProvider(provider)
      } else {
        adminAccount.disableOpenIdProvider(provider)
      }
    }

    adminAccount.save()
    res.sendStatus(HttpStatusCode.Ok)
  }),
)
adminRouter.post('/autoDownloadLimit', (req, res) => {
  const downloadLimit = Number(req.body.limit)
  if (!downloadLimit) {
    res.status(HttpStatusCode.BadRequest)
    return
  }
  if (downloadLimit < 0) {
    res.status(HttpStatusCode.BadRequest).send('Invalid download limit')
    return
  }
  adminAccount.setAutoDownloadLimit(downloadLimit)
  adminAccount.save()
  res.sendStatus(HttpStatusCode.Ok)
})
