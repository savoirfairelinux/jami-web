/*
 * Copyright (C) 2022-2024 Savoir-faire Linux Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

export const contributors = [
  'Abhishek Ojha',
  'Adrien Béraud',
  'Albert Babí',
  'Alexander Lussier-Cullen',
  'Alexandr Sergheev',
  'Alexandre Eberhardt',
  'Alexandre Lision',
  'Alexandre Viau',
  'Aline Bonnet',
  'Aline Gondim Santos',
  'Alireza Toghiani',
  'Amin Bandali',
  'AmirHossein Naghshzan',
  'Amna Snene',
  'Andreas Traczyk',
  'Anthony Léonard',
  'Brando Tovar',
  'Capucine Berthet',
  'Charles-Francis Damedey',
  'Cyrille Béraud',
  'Eden Abitbol',
  'Édric Milaret',
  'Éloi Bail',
  'Emma Falkiewitz',
  'Emmanuel Lepage-Vallée',
  'Fadi Shehadeh',
  'Franck Laurent',
  'François-Simon Fauteux-Chapleau',
  'Frédéric Guimont',
  'Guillaume Heller',
  'Guillaume Roguez',
  'Hadrien De Sousa',
  'Hugo Lefeuvre',
  'Julien Grossholtz',
  'Julien Robert',
  'Kateryna Kostiuk',
  'Kessler DuPont-Teevin',
  'Léo Banno-Cloutier',
  'Léopold Chappuis',
  'Liam Courdoson',
  'Loïc Siret',
  'Louis Maillard',
  'Mathéo Joseph',
  'Michel Schmit',
  'Mingrui Zhang',
  'Mohamed Chibani',
  'Mohamed Amine Younes Bouacida',
  'Nicolas Jäger',
  'Nicolas Reynaud',
  'Nicolas Vengeon',
  'Olivier Gregoire',
  'Olivier Soldano',
  'Patrick Keroulas',
  'Peymane Marandi',
  'Philippe Gorley',
  'Pierre Duchemin',
  'Pierre Lespagnol',
  'Pierre Nicolas',
  'Raphaël Brulé',
  'Rayan Osseiran',
  'Romain Bertozzi',
  'Saher Azer',
  'Sébastien Blin',
  'Seva Ivanov',
  'Silbino Gonçalves Matado',
  'Simon Désaulniers',
  'Simon Zeni',
  'Stepan Salenikovich',
  'Thibault Wittemberg',
  'Thomas Ballasi',
  'Trevor Tabah',
  'Vitalii Nikitchyn',
  'Vsevolod Ivanov',
  'Xavier Jouslin de Noray',
  'Yang Wang',
]

export const artworkContributors = ['Charlotte Hoffmann', 'Marianne Forget']
