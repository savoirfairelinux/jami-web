/*
 * Copyright (C) 2022-2025 Savoir-faire Linux Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

export const jamiUsernamePattern = /^[a-zA-Z0-9-_]{3,32}$/

export const inputWidth = 220

export const submitButtonWidth = 120

export const jamiLogoDefaultSize = '256px'

export const callTimeoutMs = 60_000

export const jamiAuthLogoSize = '200px'

const devApiUrl = import.meta.env.VITE_API_URL

export const isDevMode = process.env.NODE_ENV === 'development' ? true : false

let currentBaseUrl = window.location.origin

if (isDevMode) {
  currentBaseUrl = devApiUrl
}

const apiUrl: string = currentBaseUrl + '/api'

export { apiUrl }
