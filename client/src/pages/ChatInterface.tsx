/*
 * Copyright (C) 2022-2025 Savoir-faire Linux Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */
import { Stack, useMediaQuery, useTheme } from '@mui/material'
import { memo, useCallback } from 'react'
import { useDropzone } from 'react-dropzone'

import ChatInterfaceManager from '../components/ChatInterfaceManager'
import ConversationPreferences from '../components/ConversationPreferences'
import { FileDragOverlay } from '../components/Overlay'
import { useActionMessageReadContext, useActionMessageWriteContext } from '../contexts/ActionMessageProvider'
import { useConversationPreferencesStateContext } from '../contexts/ConversationPreferencesStateContext'
import { FileHandler } from '../utils/files'

const ChatInterface = () => {
  const { setFileHandlers } = useActionMessageWriteContext()
  const { fileHandlers } = useActionMessageReadContext()
  const { isOpen } = useConversationPreferencesStateContext()

  const theme = useTheme()
  const isMobile: boolean = useMediaQuery(theme.breakpoints.only('xs'))
  const isMedium: boolean = useMediaQuery(theme.breakpoints.only('sm'))

  const onFilesDrop = useCallback(
    (acceptedFiles: File[]) => {
      const newFileHandlers = acceptedFiles.map((file) => new FileHandler(file))
      setFileHandlers([...fileHandlers, ...newFileHandlers])
    },
    [fileHandlers, setFileHandlers],
  )

  const {
    getRootProps,
    getInputProps,
    open: openFilePicker,
    isDragActive,
  } = useDropzone({
    onDrop: onFilesDrop,
    noClick: true,
    noKeyboard: true,
  })

  return (
    <Stack flex={1} direction="row" overflow="hidden" spacing={0}>
      <Stack
        width={isOpen && (isMobile || isMedium) ? '0px' : '100%'}
        overflow="hidden"
        {...getRootProps()}
        paddingBottom="16px"
      >
        {isDragActive && <FileDragOverlay />}
        <input {...getInputProps()} />

        <ChatInterfaceManager openFilePicker={openFilePicker} />
      </Stack>
      <ConversationPreferences />
    </Stack>
  )
}

export default memo(ChatInterface)
